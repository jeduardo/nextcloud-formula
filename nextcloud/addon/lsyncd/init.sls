{% from "nextcloud/map.jinja" import nextcloud with context %}


{% set localipv4  = salt['grains.get']('ip_interfaces:eth0:0', {}) %}
{% set lsyncdmaster = salt['pillar.get']('nextcloud:lsyncd:master', {}) %}

lsyncd:
  pkg.installed:
    - name: lsyncd
  file.managed:
    - name: /etc/lsyncd/lsyncd.conf.lua
    - source: salt://nextcloud/addon/lsyncd/files/lsyncd.conf.lua
    - user: root
    - group: root
    - template: jinja
    - makedirs: True
{% if lsyncdmaster  == localipv4  %}
  service.running:
    - name: lsyncd
    - enable: True
{% else %}
  service.dead:
    - name: lsyncd
    - running: False
{% endif %}
    - watch:
      - file: /etc/lsyncd/lsyncd.conf.lua


ship the private key for lsync:
  file.managed:
    - name: /root/.ssh/lsyncd_ssh_key
    - source: salt://nextcloud/addon/lsyncd/files/lsyncd_ssh_key
    - mode: 600
    - makedirs: true

ship the pub key for lsync:
  file.managed:
    - name: /root/.ssh/lsyncd_ssh_key.pub
    - source: salt://nextcloud/addon/lsyncd/files/lsyncd_ssh_key.pub
    - mode: 600
    - makedirs: true

ssh_keys for lsyncd:
  ssh_auth.present:
    - user: root
    - source: salt://nextcloud/addon/lsyncd/files/lsyncd_ssh_key.pub
    - config: '%h/.ssh/authorized_keys'

ship sshconfig:
  file.managed:
    - name: /root/.ssh/config
    - source: salt://nextcloud/addon/lsyncd/files/sshconfig
    - mode: 600
    - makedirs: true


ship ssh known hosts:
  file.managed:
    - name: /root/.ssh/known_hosts
    - source: salt://nextcloud/addon/lsyncd/files/known_hosts
    - mode: 600
    - makedirs: true
