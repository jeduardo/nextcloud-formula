{% from "nextcloud/map.jinja" import nextcloud with context %}




{% set localipv4  = salt['grains.get']('ip_interfaces:eth0:0', {}) %}
{% set lsyncdmaster = salt['pillar.get']('nextcloud:lsyncd:master', {}) %}

{% if lsyncdmaster  == localipv4  %}
settings {
 logfile = "/var/log/lsyncd/lsyncd.log",
 statusFile = "/var/log/lsyncd/lsyncd.status",
 insist = 1,
}

sync {
 default.rsyncssh,
 source = "/srv/nextcloud/data/",
 host = "{{ nextcloud.lsyncd.slave }}",
 targetdir = "/srv/nextcloud/data/",
 rsync = {
 archive = true,
 owner = true,
 perms = true,
 group = true,
 compress = false,
 whole_file = true,
 _extra = {"-a"}, -- Sometimes permissions and owners isn't copied correctly so the _extra can be used for any flag in rsync
 }
}


{% else %}
settings {
 logfile = "/var/log/lsyncd/lsyncd.log",
 statusFile = "/var/log/lsyncd/lsyncd.status",
 insist = 1,
}

sync {
 default.rsyncssh,
 delete = false, -- Doesn't delete files on the remote host, in emergency case
 source = "/srv/nextcloud/data/",
 host = "{{ nextcloud.lsyncd.master }}",
 targetdir = "/srv/nextcloud/data/",
 rsync = {
 archive = true,
 owner = true,
 perms = true,
 group = true,
 compress = false,
 whole_file = true,
 _extra = {"-a"}, -- Sometimes permissions and owners isn't copied correctly so the _extra can be used for any flag in rsync
 }

}

{% endif %}
