{# Update nextcloud config according to the config map using the OCC command #}

{%- from slspath + "/map.jinja" import nextcloud with context %}

{%- for key, value in nextcloud.config.iteritems() %}

{# We set a temporary value so we can update values according to if statements #}
{%- set val = {} %}

{# Then we check whether we should handle this as a list #}
{%- if value is iterable and value is not string %}

{%- for item, value in value.iteritems() %}

# We update the array variables in two different ways. In case it's a concatenable
# list of empty entries, we update it with indexes. This will be identified when 
# there is actually no value in the config.
{%- if not value %}

update index value {{ loop.index0 }} for nextcloud config {{ key }}:
  cmd.run:
    - name: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ config:system:set {{ key }} {{ loop.index0 }} --value="{{ item }}"'
    - cwd: {{ nextcloud.install.base }}/nextcloud
    - runas: {{ nextcloud.user }}
    - onlyif: '! php7.2 {{ nextcloud.install.base }}/nextcloud/occ config:system:get {{ key }} {{ loop.index0 }} | grep "{{ item }}"'

{%- else %}
# In case there is actually a value, we will update everything named as desired.

update named value {{ item }} for nextcloud config {{ key }}:
  cmd.run:
    - name: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ config:system:set {{ key }} {{ item }} --value="{{ value }}"'
    - cwd: {{ nextcloud.install.base }}/nextcloud
    - runas: {{ nextcloud.user }}
    - onlyif: '! php7.2 {{ nextcloud.install.base }}/nextcloud/occ config:system:get {{ key }} {{ item }} | grep "{{ value }}"'
{%- endif %}

{%- endfor %}

{%- else %}

{# If it isn't a list, we do a quick sanity check for the values #}
{%- if value == None %}
{%- do val.update({key: ''}) %}
{%- elif value == True %}
{%- do val.update({key: 'true'}) %}
{%- elif value == False %}
{%- do val.update({key: 'false'}) %}
{%- else %}
{%- do val.update({key: value}) %}
{%- endif %}

{# Then we update the value as a single value #}
update single value for nextcloud config {{ key }}:
  cmd.run:
    - name: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ config:system:set {{ key }} --value="{{ val[key] }}"'
    - cwd: {{ nextcloud.install.base }}/nextcloud
    - runas: {{ nextcloud.user }}
    - onlyif: '! php7.2 {{ nextcloud.install.base }}/nextcloud/occ config:system:get {{ key }} | grep "{{ val[key] }}"'
{%- endif %}

{%- endfor %}