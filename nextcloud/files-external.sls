{# Setup LDAP authentication for Nextcloud #}

{% from "nextcloud/map.jinja" import nextcloud with context %}

{%- if nextcloud.files_external.enabled == True %}

enable files_external app:
  cmd.run:
    - name: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ app:enable files_external'
    - cwd: {{ nextcloud.install.base }}/nextcloud
    - runas: {{ nextcloud.user }}
    # The : is important, as user_ldap will only have a : if it's installed
    - onlyif: '! php7.2 {{ nextcloud.install.base }}/nextcloud/occ app:list | grep files_external:'

{%- for storage, options in nextcloud.files_external.get('storages', {}).items() %}

{% set storage_name = storage.replace('/', '') %}

# As we have no easy way to check whether a configuration has changed, we will import new
# storages only if they do not exist in the system already. Also, as we need to run the
# verification commands as the nextcloud user we will do a little improvisation here.
check if storage {{ storage }} is present:
  cmd.run:
    - name: '/bin/true'
    - cwd: {{ nextcloud.install.base }}/nextcloud
    - runas: {{ nextcloud.user }}
    - unless: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ files_external:list | grep "{{ storage }}"'

save temporary file for storage {{ storage }}:
  file.serialize:
    - name: '{{ nextcloud.data }}/storage-{{ storage_name }}.json'
    - formatter: json
    {# We calculate the mount_id and mount point automatically #}
    {%- do options.update({'mount_id': loop.index0}) %}
    {%- do options.update({'mount_point': storage}) %}
    {# We enclose the JSON file within a list to match the export format from nextcloud #}
    - dataset: {{ [options] }}
    # We add a restriction here to start processing this only if the storage is absent.
    - show_changes: True
    - backup: False
    - onchanges:
      - cmd: check if storage {{ storage }} is present

fix json serialization quirks in import for {{ storage }}:
  file.replace:
    - name: '{{ nextcloud.data }}/storage-{{ storage_name }}.json'
    - pattern: '\\\\'
    - repl: '\\'
    - onchanges:
      - file: save temporary file for storage {{ storage }}

import storage {{ storage }} if not present:
  cmd.run:
    - name: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ files_external:import "{{ nextcloud.data }}/storage-{{ storage_name }}.json"'
    - cwd: {{ nextcloud.install.base }}/nextcloud
    - runas: {{ nextcloud.user }}
    - onchanges:
      - file: fix json serialization quirks in import for {{ storage }}

cleanup import for {{ storage }}:
  file.absent:
    - name: '{{ nextcloud.data }}/storage-{{ storage_name }}.json'
    - onchanges:
      - cmd: import storage {{ storage }} if not present
    # - onchanges_in:
    #   - cmd: update file caches

{% endfor %}

# If the file caches are not updated it will take some time for nextcloud to
# display the contents of any new
# update file caches:
#   cmd.run:
#     - name: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ files:scan --all'
#     - cwd: {{ nextcloud.install.base }}/nextcloud
#     - runas: {{ nextcloud.user }}
#     # The : is important, as user_ldap will only have a : if it's installed
#     - onlyif: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ app:list | grep files_external:'


{%- else %}

disable files_external app if present:
  cmd.run:
    - name: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ app:disable files_external'
    - runas: {{ nextcloud.user }}
    - onlyif: 'php7.2 {{ nextcloud.install.base }}/nextcloud/occ app:list | grep files_external:'

{%- endif %}