{# Configuration for PHP #}

{## Session management on the application server. PHP sessions are stored in a tmpfs mounted at the operating system-specific session storage location ##}
php-session in tmpfs:
  mount.mounted:
    - name: /var/lib/php/sessions
    - device: tmpfs
    - fstype: tmpfs
    - mkmnt: True
    - opts:
      - defaults
      - noatime
      - mode=1777
    - dump: 0
    - pass_num: 0
    - persist: True

opcache_enabled:
  file.replace:
    - name: /etc/php/7.2/fpm/php.ini
    - pattern: ;opcache.enable=1
    - repl: opcache.enable=1
    - count: 1
    - append_if_not_found: True

opcache_enable_cli:
  file.replace:
    - name: /etc/php/7.2/fpm/php.ini
    - pattern: ;opcache.enable_cli=0
    - repl: opcache.enable_cli=1
    - count: 1
    - append_if_not_found: True

opcache_memory_consumption:
  file.replace:
    - name: /etc/php/7.2/fpm/php.ini
    - pattern: ;opcache.memory_consumption=128
    - repl: opcache.memory_consumption=128
    - count: 1
    - append_if_not_found: True

opcache_max_accelerated_files:
  file.replace:
    - name: /etc/php/7.2/fpm/php.ini
    - pattern: ;opcache.max_accelerated_files=10000
    - repl: opcache.max_accelerated_files=10000
    - count: 1
    - append_if_not_found: True

opcache_interned_strings:
  file.replace:
    - name: /etc/php/7.2/fpm/php.ini
    - pattern: ;opcache.interned_strings_buffer=8
    - repl: opcache.interned_strings_buffer=8
    - count: 1
    - append_if_not_found: True

opcache_save_comments:
  file.replace:
    - name: /etc/php/7.2/fpm/php.ini
    - pattern: ;opcache.save_comments=1
    - repl: opcache.save_comments=1
    - count: 1
    - append_if_not_found: True

opcache_revalidate_freq:
  file.replace:
    - name: /etc/php/7.2/fpm/php.ini
    - pattern: ;opcache.revalidate_freq=2
    - repl: opcache.revalidate_freq=1/opc
    - count: 1
    - append_if_not_found: True

php-fpm enviroments HOSTNAME:
  file.replace:
    - name: /etc/php/7.2/fpm/pool.d/www.conf
    - pattern: ;env[HOSTNAME] = $HOSTNAME
    - repl: env[HOSTNAME] = $HOSTNAME
    - count: 1
    - append_if_not_found: True

php-fpm enviroments PATH:
  file.replace:
    - name: /etc/php/7.2/fpm/pool.d/www.conf
    - pattern: ;env[PATH] = /usr/local/bin:/usr/bin:/bin
    - repl: env[PATH] = /usr/local/bin:/usr/bin:/bin
    - count: 1
    - append_if_not_found: True

php-fpm enviroments TMP:
  file.replace:
    - name: /etc/php/7.2/fpm/pool.d/www.conf
    - pattern: ;env[TMP] = /tmp
    - repl: env[TMP] = /tmp
    - count: 1
    - append_if_not_found: True

php-fpm enviroments TEMPDIR:
  file.replace:
    - name: /etc/php/7.2/fpm/pool.d/www.conf
    - pattern: ;env[TMPDIR] = /tmp
    - repl: env[TMPDIR] = /tmp
    - count: 1
    - append_if_not_found: True

php-fpm enviroments TEMP:
  file.replace:
    - name: /etc/php/7.2/fpm/pool.d/www.conf
    - pattern: ;env[TEMP] = /tmp
    - repl: env[TEMP] = /tmp
    - count: 1
    - append_if_not_found: True
